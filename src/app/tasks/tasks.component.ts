import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from "rxjs";

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {

  readonly url='http://localhost:8081/api/v1/processor/tasks';

  tasks: Array<any>;
  interval:any

  constructor(private http: HttpClient) { }

  getTasks(): Observable<any> {
    return this
      .http
      .get(this.url);
  }

  refreshTasks(){
    this.getTasks().subscribe(tasks => {
      this.tasks = tasks;
    })
  }

  ngOnInit() {
    this.refreshTasks();
    this.interval=setInterval(()=>{
      this.refreshTasks();
    },500)
  }

}
