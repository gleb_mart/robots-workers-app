# RobotsWorkersUi

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.6.

## Запуск
1. Скачайте проект
2. Скачать и установить Git https://git-scm.com/downloads 
3. Скачать и установить https://nodejs.org/en/ 
4. В командной троке Git Bash выполнить команды: 
`npm install -g @angular/cli` , 
`npm install —save-dev @angular-devkit/build-angular `
5. Запустить консоль Git Bash в папке проекта, чтобы выполнять команды внутри директории 
6. Выполните команду `ng serve` чтобы развернуть проект
7. Перейдите по ссылке `http://localhost:4200/`
8. Запустите серверную часть `Robots Workers` - отдельный проект на Java

## Возможности
Пока что можно только наблюдать как выполняются задачи
